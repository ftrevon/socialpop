import os
from data_mining import *
from visulization import *
from scripts import app_json

def run(username):


	create_dir(username)
	exists = is_finished(username)
	if (exists == False):
		some_lsit = []
		### DATA MINING
		get_primary_user(username)
		get_user_tweets(username,some_lsit)
		some_lsit = []
		get_user_followers_ids(username, some_lsit, -1)
		get_user_followers_info(username, get_user_info_dict(username), 0)
		get_user_mentions(username)


		### VISULIZATION
		mentions_word_cloud(username)
		mentions_network(username)
		users_top_posts(username)
		followers_popularity_circle(username,get_user_followers_info_dict(username))
		followers_gender(username,get_user_followers_info_dict(username))
		followers_acitivity(username,get_user_followers_info_dict(username))
		mentions_sentimental_histogram(username)

		### FINISHED
		finished(username)
	else:
		#TODO - just send finished report.
		finished(username)
		print "it is already done. Go to the folder: ../Social_graphs/Users/" + username


def create_dir(username):
	directory = "../Social_graphs/Users/" + username
	if not os.path.exists(directory):
	    os.makedirs(directory)


def check_user_data(username):
	#todo
	return 


def is_finished(username):
	directory = "../Social_graphs/Users/{0}/finished.txt".format(username)
	if os.path.exists(directory) == False:
		exists = False
	else:
		exists = True
	return exists


def finished(username):
	done_string = "Now done with collecting data from {0}".format(username)
	with open('../Social_graphs/Users/{0}/finished.txt'.format(username),'w') as f:
		json.dump(done_string,f)
	user_dict = get_user_dict(username)
	app_json(username,user_dict)

user_dict = get_user_dict('larsloekke')
app_json('larsloekke',user_dict)