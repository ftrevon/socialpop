import re
import json
from twit_connect import *
import time

user_dir = "../Social_graphs/Users/{0}/"


def get_primary_user(username):

    ''' Retrieves the dict of info from Twitter API of our politician.

        Parameters:
        username     eg. for: twitter.com/larsloekke it would be "larsloekke"
    '''
    # iterates over our different twitter api keys untill it gets a connection.
    # When it gets a connection it looksuip the user via the twitter_api module

    for a in range(0, twitter_api_keys):
        try:
            twitter_api = twitter_connect(a)
            user_id_dict = twitter_api.users.lookup(screen_name=username, timeout=1)
            break
        except:
            print "sorry we ran out of keys. Wait 15 minutes"
            if(a < (twitter_api_keys-1)):
                pass
            else:
                time.sleep(15*60)
                get_user_id(username)
    #dumps the user info in a folder specific as a json file
    dump_dir = user_dir + "target_user.json"
    with open(dump_dir.format(username) , 'w') as f:
            json.dump(user_id_dict[0], f)

            # it is a list of dictionaries with len 1
            #so we just remove the list brackets with the "[0]"

def get_user_dict(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format(username)  + "target_user.json", 'r') as f:
        user_id_dict = json.load(f)
    return user_id_dict


def get_user_id(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format(username)  + "target_user.json", 'r') as f:
        user_id_dict = json.load(f)
    user_id = user_id_dict['id']
    return user_id


def get_user_statuses_count(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format(username)  + "target_user.json", 'r') as f:
        user_id_dict = json.load(f)
    user_statuses_count = user_id_dict['statuses_count']
    return user_statuses_count


def get_user_followers_count(username):
    ''' Classic get_method. Retrieves data from the:
        stored dict made in get_primary_user

        Parameters:
        username  eg. for: twitter.com/larsloekke it would be "larsloekke"
    '''
    with open(user_dir.format(username) + "target_user.json", 'r') as f:
        user_id_dict = json.load(f)
	followers_count = user_id_dict['followers_count']
	return followers_count


def get_user_followers_ids(username, id_list, cursor):
    iters = -((get_user_followers_count(username)/5000)+1)
    for b in range(iters, 0):
        for a in range(0,twitter_api_keys):
            twitter_api = twitter_connect(a)
            try:
                followers_batch = twitter_api.followers.ids(screen_name=str(username),skip_status='true', include_user_entities='false', cursor = str(cursor))
                cursor = followers_batch['next_cursor']
                print "Succesfully caught: " +str((b-iters)+1) + " out of: " + str(-iters)
                break
            except:
                print "Stopped, and caught: " +str((b-iters)) + " out of: " + str(-iters)
                if( a < (twitter_api_keys-1)):
                    pass
                else:
                    print "Waiting for 15 min."
                    time.sleep(15*60)
                    get_user_followers_ids(username,id_list,cursor)
                    break
        id_list.extend(followers_batch['ids'])  
    inner_dict_ids = {}
    user_info = {}
    for a in id_list:
        inner_dict_ids[a] = ""
    user_info['followers'] = inner_dict_ids
    with open(user_dir.format(username)  + "user_info.json",'w') as f:
        json.dump(user_info,f)
    print "##########Now done with getting followers ids"


def get_user_info_dict(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format(username)  + "user_info.json", 'r') as f:
        user_info_dict = json.load(f)
    return user_info_dict


def get_user_followers_info(username, users_dict, cursor):
    iters = -((get_user_followers_count(username)/100)+1 -cursor)
    temp_users_dict_keys = users_dict['followers'].keys()
    for b in range(iters, 0):
        for a in range(0,twitter_api_keys):
            twitter_api = twitter_connect(a)
            try:
                current_interval = (-b*(100))
                usernames_as_strings = temp_users_dict_keys[(current_interval-100):current_interval]
                usernames = map(int,usernames_as_strings)
                followers_batch = twitter_api.users.lookup(user_id=usernames)
                print "Succesfully caught: " +str((b-iters)+1) + " out of: " + str(-iters)
                break
            except:
                if( a < (twitter_api_keys-1)):
                    pass
                else:
                    print "Stopped, and caught: " +str((b-iters)) + " out of: " + str(-iters)
                    print "Waiting for 15 min."
                    time.sleep(15*60)
                    get_user_followers_info(username,users_dict,-(iters-b))
                    break
        for a in followers_batch:
            users_dict['followers'][a['id']] = a
    user_followers_info_dict = users_dict
    with open(user_dir.format(username) + "user_followers_info_dict.json",'w') as f:
        json.dump(user_followers_info_dict,f)
    print "##########Now done with getting followers user_info"


def get_user_followers_info_dict(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format(username)  + "user_followers_info_dict.json", 'r') as f:
        user_info_dict = json.load(f)
    return user_info_dict


def get_user_tweets(username,id_list):
    iters = -((get_user_statuses_count(username)/200)+1)
    for b in range(iters, 0):
        for a in range(0,twitter_api_keys):
            twitter_api = twitter_connect(a)
            try:
                if b == min(range(iters, 0)):
                    tweets_batch = twitter_api.statuses.user_timeline(screen_name=str(username),count = 200, include_rts=1, since_id = 2)
                else:
                    tweets_batch = twitter_api.statuses.user_timeline(screen_name=str(username),count = 200, include_rts=1, max_id = cursor)
                if(b != -1):
                    cursor = tweets_batch[len(tweets_batch)-1]['id']
                print "Succesfully caught: " +str((b-iters)+1) + " out of: " + str(-iters)
                break
            except Exception,e :
                print e
                if( a < (twitter_api_keys-1)):
                    pass
                else:
                    print "Stopped, and caught: " +str((b-iters)) + " out of: " + str(-iters)
                    print "Waiting for 15 min."
                    time.sleep(15*60)
                    get_user_tweets(username,id_list,cursor)
                    break
        id_list.extend(tweets_batch)  
    user_tweets = {}
    for a in id_list:
        user_tweets[a['id']] = a
    with open(user_dir.format(username)  + "user_tweets.json",'w') as f:
        json.dump(user_tweets,f)
    print "##########Now done with getting users tweets"


def get_user_tweets_dict(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format('larsloekke')  + "user_tweets.json",'r') as f:
        user_tweets = json.load(f)
    return user_tweets


def get_user_mentions(username):
    all_mentions_dict = {}
    id_list = []
    tag = '%40' + username
    twitter_api = twitter_connect(0)
    mentions_batch = twitter_api.search.tweets(since_id = 0, q=tag,count=100,include_entities=1)
    try:
        next_batch = mentions_batch['search_metadata']['next_results']
        next_max_id = re.findall ('max_id=(.*?)&',next_batch)
        id_list.extend(mentions_batch['statuses'])
        while (len(mentions_batch['statuses']) == 100):
            mentions_batch = twitter_api.search.tweets(max_id=int(next_max_id[0]),q=tag,count=100,include_entities=1)
            try:
                next_batch = mentions_batch['search_metadata']['next_results']
            except KeyError:
                id_list.extend(mentions_batch['statuses'])
                break
            next_max_id = re.findall ('max_id=(.*?)&',next_batch)
            id_list.extend(mentions_batch['statuses'])
    except KeyError:
        mentions_dict = {}
        for a in range(0,len(id_list)):
            mentions_dict[id_list[a]['id']] = id_list[a]
        with open(user_dir.format(username) + "user_mentions_dict.json",'w') as f:
                json.dump(mentions_dict,f)
    mentions_dict = {}
    for a in range(0,len(id_list)):
        mentions_dict[id_list[a]['id']] = id_list[a]
    with open(user_dir.format(username) + "user_mentions_dict.json",'w') as f:
            json.dump(mentions_dict,f)


def get_user_mentions_dict(username):
    ''' Classic get_method. Retrieves data from the
        stored dict made in get_primary_user

        Parameters:
        username        eg. for: twitter.com/larsloekke
                        it would be "larsloekke"
    '''
    with open(user_dir.format(username)  + "user_mentions_dict.json",'r') as f:
        jsondict = json.load(f)
    return jsondict
##RUNNING!!!
#get_user_followers_info('larsloekke', get_user_info_dict('larsloekke'), 0)
#some_lsit = []
#get_user_followers_ids('larsloekke,', some_lsit, -1)
#get_user_tweets('larsloekke',some_lsit, 2)

#get_primary_user('larsloekke')