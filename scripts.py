from wordcloud import WordCloud
from scipy.misc import imread
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
wnl = WordNetLemmatizer()
import numpy as np
import re
import nltk
import xlrd
from sklearn.feature_extraction.text import CountVectorizer
import datetime as dt
import json

def plot_wordcloud(cloudstring):
    twitter_mask = imread('../Social_graphs/fig/twitter_mask.png', flatten=True)


    wordcloud = WordCloud(
                          font_path='../Social_graphs/fig/CabinSketch-Bold.ttf',
                          stopwords="",
                          background_color='white',
                          width=1800,
                          height=1400,
                          mask=twitter_mask
                ).generate(cloudstring)
    return wordcloud


def tokenization(content,username):
    
    stopwords_set = set(stopwords.words('danish') + stopwords.words('English'))
    # Declaring variable 'count', used for progress bar (time estimation) 
    count = 1
    
    doc = xlrd.open_workbook('../Social_graphs/Data_for_statistics/danish_male_names.xls').sheet_by_index(0)
    mens_names = doc.col_values(0,1,14821)
    doc = xlrd.open_workbook('../Social_graphs/Data_for_statistics/danish_female_names.xls').sheet_by_index(0)
    female_names = doc.col_values(0,1,18223)
    all_names = mens_names + female_names
    

    # Declaring an empty array ' temp_list', used for storing the tokenized words
    temp_list = []
    
    for b in range(len(content.values())):
        
        # PROGRESS BAR
        tenperc = int(np.mod(len(content.values())/(count*10),len(content.values())))
        if(tenperc == b):
            print "Progress: " + str(((1.1-count)*100)) + " %"
            count = count - 0.1
        query = content.values()[b]
        querywords = query.split()
        home_made_stopwords = re.findall('#\w+',query)
        home_made_stopwords.extend(re.findall('@\w+',query))
        home_made_stopwords.extend(re.findall('@\w+:',query))
        home_made_stopwords.extend(['RT','http','https','danmark',username])
        home_made_stopwords.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', query))
        for a in range(0,len(home_made_stopwords)):
            home_made_stopwords[a] = home_made_stopwords[a].lower()
        resultwords  = [word for word in querywords if word.lower() not in home_made_stopwords]
        result = ' '.join(resultwords)
        # TOKENIZATION
        temp_set = set(nltk.word_tokenize(result))
        for a in temp_set:
            # consider iterating over this instead, so you do not get front and last names: nltk.word_tokenize(' '.join(all_phil_names))
            
            # setting everything to lower-case and excluding all stop words
            if a.lower() in stopwords_set:
                continue
            
            # excluding all philosopher names
            if a in all_names:
                continue
                
            # excluding numbers and punctuation by only taking letters   
            if not a.isalpha():
                continue
            
            # storing the words in the empty array 'temp_list'
            temp_list.append(wnl.lemmatize(a.lower()))
    temp_list  = [word for word in temp_list if word.lower() not in home_made_stopwords]
    return temp_list


def getAnewScores(commentList,MyVectorizer, ANEWlist):
    ANEWarray = MyVectorizer.fit_transform(commentList).toarray()
    commentANEWscores = {}
    for commentNumber in range(ANEWarray.shape[0]):
        ValenceValue = 0
        ArousalValue = 0
        DominanceValue = 0
        totalWordCount = 0
        for i in range(len(ANEWarray[commentNumber].tolist())):
            wordName = MyVectorizer.vocabulary[i]
            wordAmount = ANEWarray[commentNumber].tolist()[i]
            if wordAmount != 0:
                #calculate Valence, Arousal and Dominance scores for a comment
                ValenceValue += ANEWlist[wordName][0]*wordAmount
                ArousalValue += ANEWlist[wordName][1]*wordAmount
                DominanceValue += ANEWlist[wordName][2]*wordAmount
                totalWordCount += wordAmount
        if totalWordCount == 0:
            totalANEWscore = None
        else:
            totalANEWscore = (ValenceValue + ArousalValue + DominanceValue)/totalWordCount
        commentANEWscores[commentNumber] = totalANEWscore
    return commentANEWscores


def month_to_number(tm):
    a = None
    if tm == 'jan':
        a = 1
    elif tm == 'feb':
        a = 2
    elif tm == 'mar':
        a = 3
    elif tm == 'apr':
        a = 4
    elif tm == 'may':
        a = 5
    elif tm == 'jun':
        a = 6
    elif tm == 'jul':
        a = 7
    elif tm == 'aug':
        a = 8   
    elif tm == 'sep':
        a = 9
    elif tm == 'oct':
        a = 10
    elif tm == 'nov':
        a = 11
    elif tm == 'dec':
        a = 12
    return int(a)


def twitter_date_to_datetime(a, user_mentions):
    month = month_to_number(user_mentions.items()[a][1]['created_at'][4:7].lower())
    day = user_mentions.items()[a][1]['created_at'][8:10]
    hour = user_mentions.items()[a][1]['created_at'][11:13]
    minute = user_mentions.items()[a][1]['created_at'][14:16]
    second = user_mentions.items()[a][1]['created_at'][17:19]
    year = user_mentions.items()[a][1]['created_at'][26:30]
    one_mention_date = dt.datetime(int(year),
                                    int(month),
                                    int(day),
                                    int(hour),
                                    int(minute))
    return one_mention_date


def app_json(username, user_dict):
    app_dict = {}

    app_dict['name'] = user_dict['name']
    app_dict['twitter_handel'] = user_dict['screen_name']
    app_dict['about'] = user_dict['description']
    app_dict['num_followers'] = str(user_dict['followers_count'])
    app_dict['num_tweets'] = str(user_dict['statuses_count'])
    app_dict['num_mentions'] = str(100)
    app_dict['age'] = str(51)



    temp_list = []
    temp_dict = {}



    temp_dict['title'] = "Pie Chart of followers activity"
    temp_dict['image_name'] = "followers_acitivity_circle.png"
    temp_dict['description'] = "This pie chart uses our own optimized measure based on the many public standards on twitter activity. Feel free to contact us, if you need information on your most active followers, or want an specific analysis on them."
    temp_list.append(temp_dict)
    temp_dict = {}

    temp_dict['title'] = "Pie Chart of followers gender"
    temp_dict['image_name'] = "followers_gender_circle.png"
    temp_dict['description'] = "The best estimate you get of your followers gender distribution. Since you do not provide gender to twitter, this is based on your followers first name. 18.3% of your followers did not provide real names. Their gender remains unknown."
    temp_list.append(temp_dict)
    temp_dict = {}

    temp_dict['title'] = "50 people mentioning you most"
    temp_dict['image_name'] = "mentions_network.png"
    temp_dict['description'] = "This network shows who mentions your most on twitter. The size of each red circle is according to how many times you are mentioned."
    temp_list.append(temp_dict)
    temp_dict = {}

    temp_dict['title'] = "Mentions sentimental score over time"
    temp_dict['image_name'] = "mentions_sentimental.png"
    temp_dict['description'] = "We can determine how much each of your mentions are positive or negative. This figure shows the average mood of the mentions in the given time period."
    temp_list.append(temp_dict)
    temp_dict = {}

    temp_dict['title'] = "Mentions Word Cloud"
    temp_dict['image_name'] = "mentions_word_cloud.png"
    temp_dict['description'] = "It is nice knowing what people say about your on twitter, but it gets insurmountable with many mentions and tweets. This is a word cloud with the most important words from the people mentioning you. The importancy is according to size."
    temp_list.append(temp_dict)
    temp_dict = {}

    temp_dict['title'] = "Pie chart over the popularity of followers"
    temp_dict['image_name'] = "pop_circle.png"
    temp_dict['description'] = "This Pie Chart gives a nice hint of how important your followers are."
    temp_list.append(temp_dict)
    temp_dict = {}

    temp_dict['title'] = "Top 10 most import posts"
    temp_dict['image_name'] = "UsersTopPosts.png"
    temp_dict['description'] = "There is always a reason for a post to be the most popular. This is a overview of your 10 most popular ones, and whether they are retweets or not."
    temp_list.append(temp_dict)

    app_dict['visualisations'] = temp_list


    with open("../Social_graphs/Users/{0}/app_file.json".format(username) , 'w') as f:
            json.dump(app_dict, f)