import pylab as plt
from data_mining import get_user_followers_info_dict, get_user_tweets_dict, get_user_mentions_dict
import operator
import networkx as nx
from collections import Counter
import re
from scripts import *
import csv
import datetime as dt

user_dir = "../Social_graphs/Users/{0}/"


#@deprecated
def followers_location(username):
    Location_list = []
    for a in user_info_dict['followers']:
        if user_info_dict['followers'][a] == "" or user_info_dict['followers'][a] == None:
            pass
        else:
            try:
                klbm = user_info_dict['followers'][a]['location']
                if(klbm.isalpha() == True):
                    Location_list.append(user_info_dict['followers'][a]['location'].lower())
            except:
                pass


def followers_popularity_circle(username,user_info_dict):
    popularity_list = []
    for a in user_info_dict['followers']:
        if user_info_dict['followers'][a] == "":
            pass
        else:
            popularity_list.append(user_info_dict['followers'][a]['followers_count'])

    bar_chart_dict = {}
    bar_chart_dict['51<'] = 0
    bar_chart_dict['51-200'] = 0
    bar_chart_dict['201-1000'] = 0
    bar_chart_dict['1001-10000'] = 0
    bar_chart_dict['>10000'] = 0

    for a in popularity_list:
        if a < 51:
            bar_chart_dict['51<'] = bar_chart_dict['51<'] + 1
        if a > 50 and a < 201:
            bar_chart_dict['51-200'] = bar_chart_dict['51-200'] + 1
        if a > 200 and a < 1001:
            bar_chart_dict['201-1000'] = bar_chart_dict['201-1000'] + 1
        if a > 1000 and a < 10001:
            bar_chart_dict['1001-10000'] = bar_chart_dict['1001-10000'] + 1
        if a > 10000:
            bar_chart_dict['>10000'] = bar_chart_dict['>10000'] + 1
    bar_chart_dict['51<'] = (float(bar_chart_dict['51<'])/float(len(popularity_list)))*100
    bar_chart_dict['51-200'] = (float(bar_chart_dict['51-200'])/float(len(popularity_list)))*100
    bar_chart_dict['201-1000'] = (float(bar_chart_dict['201-1000'])/float(len(popularity_list)))*100
    bar_chart_dict['1001-10000'] = (float(bar_chart_dict['1001-10000'])/float(len(popularity_list)))*100
    bar_chart_dict['>10000'] = (float(bar_chart_dict['>10000'])/float(len(popularity_list)))*100

    # make a square figure and axes
    plt.figure(1, figsize=(6,6))
    ax = plt.axes([0.1, 0.1, 0.8, 0.8])

    # The slices will be ordered and plotted counter-clockwise.
    labels = bar_chart_dict.keys()
    fracs = bar_chart_dict.values()
    colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral', '#D933FC']
    store_pie = plt.pie(fracs, colors = colors, labels=labels,
                    autopct='%1.1f%%', startangle=90)
                    # The default startangle is 0, which would start
                    # the Frogs slice on the x-axis.  With startangle=90,
                    # everything is rotated counter-clockwise by 90 degrees,
                    # so the plotting starts on the positive y-axis.

    for pie_wedge in store_pie[0]:
        pie_wedge.set_edgecolor('white')

    plt.title('Followeres popularity', bbox={'facecolor':'0.9', 'pad':10})
    plt.savefig(user_dir.format(username) + 'pop_circle.png', transparent=True, format='png',)


def followers_gender(username,user_info_dict):
    Names_list = []
    for a in user_info_dict['followers']:
        if user_info_dict['followers'][a] == "" or user_info_dict['followers'][a] == None:
            pass
        else:
            try:
                Names_list.append(user_info_dict['followers'][a]['name'].split()[0])
            except:
                Names_list.append(user_info_dict['followers'][a]['name'])

    # Load xls sheet with data
    doc = xlrd.open_workbook('../Social_graphs/Data_for_statistics/danish_male_names.xls').sheet_by_index(0)

    mens_names = doc.col_values(0,1,14821)

    doc = xlrd.open_workbook('../Social_graphs/Data_for_statistics/danish_female_names.xls').sheet_by_index(0)

    female_names = doc.col_values(0,1,18223)
    #max index = 14821

    men_count = 0
    women_count = 0
    other_count = 0
    for a in Names_list:
        if a in mens_names:
            men_count +=1
        elif a in female_names:
            women_count +=1
        else:
            other_count += 1
    print "men_count: " + str(men_count)
    print "women_count: " + str(women_count)
    print "other_count: " + str(other_count)

    men_count = float(men_count)
    women_count = float(women_count)

    # make a square figure and axes
    plt.figure(2, figsize=(6,6))
    ax = plt.axes([0.1, 0.1, 0.8, 0.8])

    # The slices will be ordered and plotted counter-clockwise.
    labels = 'women','men'
    fracs = (women_count/(women_count+men_count))*100,(men_count/(men_count+women_count))*100
    explode=(0, 0,)
    colors = ['yellowgreen', 'lightcoral']
    store_pie = plt.pie(fracs, explode=explode, colors = colors, labels=labels,
                    autopct='%1.1f%%', startangle=90)
                    # The default startangle is 0, which would start
                    # the Frogs slice on the x-axis.  With startangle=90,
                    # everything is rotated counter-clockwise by 90 degrees,
                    # so the plotting starts on the positive y-axis.

    for pie_wedge in store_pie[0]:
        pie_wedge.set_edgecolor('white')

    plt.title('Followeres gender', bbox={'facecolor':'0.9', 'pad':10})
    plt.savefig(user_dir.format(username) + 'followers_gender_circle.png', transparent=True, format='png',bbox_inches='tight')


def followers_acitivity(username,user_info_dict):
    ############# COOOORDINATEOSSE
    act_list = []
    for a in user_info_dict['followers']:
        if user_info_dict['followers'][a] == "" or user_info_dict['followers'][a] == None:
            pass
        else:
            try:
                act_count1 = user_info_dict['followers'][a]['favourites_count']
                act_count2 = user_info_dict['followers'][a]['followers_count']
                act_count3 = user_info_dict['followers'][a]['friends_count']
                act_count4 = user_info_dict['followers'][a]['status']['retweet_count']
                act_val = (act_count1 *0.3  + act_count2*0.01  + act_count3*0.05 + act_count4*0.01 )
                act_list.append(act_val)
            except:
                pass

    bar_chart_dict = {}
    bar_chart_dict['not very active'] = 0
    bar_chart_dict['abit active'] = 0
    bar_chart_dict['regularly active'] = 0
    bar_chart_dict['Really active'] = 0

    for a in act_list:
        if a < 5:
            bar_chart_dict['not very active'] = bar_chart_dict['not very active'] + 1
        if a > 5 and a < 10:
            bar_chart_dict['abit active'] = bar_chart_dict['abit active'] + 1
        if a > 10 and a < 50:
            bar_chart_dict['regularly active'] = bar_chart_dict['regularly active'] + 1
        if a > 50:
            bar_chart_dict['Really active'] = bar_chart_dict['Really active'] + 1
    bar_chart_dict['not very active'] = (float(bar_chart_dict['not very active'])/float(len(act_list)))*100
    bar_chart_dict['abit active'] = (float(bar_chart_dict['abit active'])/float(len(act_list)))*100
    bar_chart_dict['regularly active'] = (float(bar_chart_dict['regularly active'])/float(len(act_list)))*100
    bar_chart_dict['Really active'] = (float(bar_chart_dict['Really active'])/float(len(act_list)))*100

    # make a square figure and axes
    plt.figure(3, figsize=(6,6))

    # The slices will be ordered and plotted counter-clockwise.
    labels = bar_chart_dict.keys()
    fracs = bar_chart_dict.values()
    explode=(0, 0, 0, 0)
    colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral']
    store_pie = plt.pie(fracs, explode=explode, colors = colors, labels=labels,
                    autopct='%1.1f%%',  startangle=90)
                    # The default startangle is 0, which would start
                    # the Frogs slice on the x-axis.  With startangle=90,
                    # everything is rotated counter-clockwise by 90 degrees,
                    # so the plotting starts on the positive y-axis.

    for pie_wedge in store_pie[0]:
        pie_wedge.set_edgecolor('white')
    plt.title('Followeres profile-activity', bbox={'facecolor':'0.9', 'pad':10})

    plt.savefig(user_dir.format(username) + 'followers_acitivity_circle.png', transparent=True, format='png', bbox_inches='tight')


def users_top_posts(username):
    #Getting the dict from the datamining class
    user_tweets = get_user_tweets_dict(username)
    
    #finding the top tweets
    top_tweets = {}
    for a in range(0,len(user_tweets)):
        tw_fc = user_tweets.values()[a]['favorite_count']
        tw_rc = user_tweets.values()[a]['retweet_count']
        measure = top_tweets[user_tweets.values()[a]['id']] = tw_fc*0.25 + tw_rc
        user_tweets.values()[a] = measure
    top_tweets = dict(sorted((top_tweets.items()), key=operator.itemgetter(1), reverse=True)[0:11])

    #Creating a new dict, which is just a transformation of top_tweets
    count = 0
    plot_tweets = {}
    for a in top_tweets:
        x_tricks_sub_val1 = user_tweets[str(top_tweets.keys()[count])]['created_at'][4:10]
        x_tricks_sub_val2  = user_tweets[str(top_tweets.keys()[count])]['created_at'][25:30]
        x_tricks = x_tricks_sub_val1 + x_tricks_sub_val2
        #print x_tricks
        plot_tweets[top_tweets[a]] = x_tricks
        count += 1
    #plotting shitz.

    #fig
    fig = plt.figure(4)
    fig.set_size_inches(14, 8)

    #Labels
    _ = plt.xticks(range(1,11), plot_tweets.values(), rotation=25)
    plt.ylabel("Popularity")
    plt.title("{0}'s: Top 10 most popular posts".format(username), fontsize=14, fontweight='bold')
    plt.ylim((0,max(plot_tweets.keys()) + max(plot_tweets.keys())*0.08))
    #texts
    ax = fig.add_subplot(111)
    for a in range(0,10):
        try:
            user_tweets[str(top_tweets.keys()[a])]['retweeted_status']
            ax.text(a+2.25, plot_tweets.keys()[a+1] + max(plot_tweets.keys())*0.035, 'RT', style='italic',
            bbox={'facecolor':'red', 'alpha':0.5, 'pad':14})
        except KeyError:
            pass
    #data_values as bars
    plt.bar(range(1,11),plot_tweets.keys())

    #saving the fig at user_dir/pop_circle
    plt.savefig(user_dir.format(username) + 'UsersTopPosts.png', transparent=True, format='png',bbox_inches='tight')


def mentions_network(username):
    user_mentions = get_user_mentions_dict(username)
    if user_mentions == {}:
        print "No mentions."
    else:
        top50networklist = []
        for a in range(0,len(user_mentions)):
            top50networklist.append(user_mentions.values()[a]['user']['screen_name'])
        top50networklist = Counter(top50networklist).most_common()[:25]
        top50networkdict = {}
        for a in range(0,len(top50networklist)):
            top50networkdict[top50networklist[a][0]] = top50networklist[a][1]
        G = nx.Graph()
        for a in top50networkdict:
            G.add_node(a)
            G.add_edge(a,username)
        nodesizes = [v * 75 for v in top50networkdict.values()]

        #nx.draw_graphviz(G, nodelist=top50networkdict.keys(), node_size=[v * 500 for v in top50networkdict.values()])
        G_pos = nx.graphviz_layout(G)
        plt.figure(5).set_size_inches(15,15)    
        plt.axis('off')

        nodes = nx.draw_networkx_nodes(G, G_pos,node_size=nodesizes)
        nodes.set_edgecolor('r')
        nx.draw_networkx_edges(G, G_pos)
        nx.draw_networkx_nodes(G, pos=G_pos, nodelist=[username], node_color='b',node_size = max(nodesizes)*1.5)
        nx.draw_networkx_labels(G,G_pos, font_size = 14,font_color = '#33cc33', font_weight='bold');

        plt.savefig(user_dir.format(username) + 'mentions_network.png', transparent=True, format='png', dpi=1000,bbox_inches='tight')


def mentions_word_cloud(username):
    user_mentions = get_user_mentions_dict(username)
    if user_mentions == {}:
        print "No mentions."
    else:
        for a in user_mentions:
            user_mentions[a] = user_mentions[a]['text']

        mentions_tokenized = Counter(tokenization(user_mentions,username))

        cloud_string =""

        for a,b in mentions_tokenized.items():
            if(len(a.strip()) > 1):
                cloud_string = cloud_string + (a + " ")*int(np.log(b))*20
        plt.figure(6)
        plt.title("people who mentioned: "+ username + "'s most used words")
        wordcloud = plot_wordcloud(cloud_string)
        plt.imshow(wordcloud)
        plt.axis("off")
        plt.savefig(user_dir.format(username) + 'mentions_word_cloud.png', transparent=True, format='png', dpi=1000,bbox_inches='tight')


def mentions_sentimental_histogram(username):
    user_mentions = get_user_mentions_dict(username)

    if user_mentions == {}:
        print "No mentions"

    else:
        for a in range(0,len(user_mentions)):
            user_mentions.items()[a][1]['created_at'] = twitter_date_to_datetime(a, user_mentions)

        timetester = []
        for a in range(0,len(user_mentions)):
            timetester.append(user_mentions.items()[a][1]['created_at'].toordinal())
        timetester = sorted(timetester)

        text_dict_temp = {}
        for a in range(0,len(user_mentions)):
            text_dict_temp[user_mentions.items()[a][1]['text']] = user_mentions.items()[a][1]['created_at']

        ###### INIT ANEW METHOD:
        ANEWlist = {}
        FirstTime = True
        file_name = '../Social_graphs/Data_for_statistics/Ratings_Warriner_et_al.csv'
        with open(file_name, 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                #print ', '.join(row)

                if FirstTime: #dont do this for the first row
                    FirstTime = False
                else:
                    Name = row[1]
                    Valence = float(row[2]) - 5
                    Arousal = float(row[5]) - 5
                    Dominance = float(row[8]) - 5

                    ANEWlist[Name] = [Valence, Arousal, Dominance]

        MyVectorizer = CountVectorizer(vocabulary = ANEWlist.keys())

        plot_anew_means = {}
        for a in Counter(timetester):
            iter_date = dt.date.fromordinal(a)
            anew_dict = {}
            for b in text_dict_temp:
                if(text_dict_temp[b].date() == iter_date):
                    anew_dict[b] = ""
            ANEWscoreList = getAnewScores(anew_dict.keys(), MyVectorizer, ANEWlist)
            for c in ANEWscoreList:
                if ANEWscoreList[c] == None:
                    ANEWscoreList[c] = 0
                else:
                    pass
            plot_anew_means[iter_date] = np.mean(ANEWscoreList.values())
            print str(a - (Counter(timetester).keys()[0]-1)) + " out of: " + str(len(Counter(timetester))) + " is done"


        dates = sorted(plot_anew_means.keys())
        vals = plot_anew_means.values()
        norm_vals = []
        for a in vals:
            norm_vals.append((np.abs(a) + (1 - max(np.abs(vals))))*0.4 + 0.6)

        fig = plt.figure(7)
        fig.set_size_inches(14, 8)
        for a in range(0,len(vals)):
            if(vals[a] >= 0):
                color = 'b'
            else:
                color = 'r'
            plt.bar(a, vals[a], 0.65,
                             alpha=np.abs(norm_vals[a]),
                         color=color)
        plt.xticks(range(0,len(vals)), dates, rotation=25)
        plt.ylim(-1,1)
        plt.xticks
        plt.ylabel("Sentimental Score")
        plt.savefig(user_dir.format(username) + 'mentions_sentimental.png', transparent=True, format='png',bbox_inches='tight')

### RUNNING STUFF
#mentions_word_cloud('larsloekke',)
#mentions_network('larsloekke')
#users_top_posts('larsloekke')
#followers_popularity_circle('larsloekke',get_user_followers_info_dict('larsloekke'))
#followers_gender('larsloekke',get_user_followers_info_dict('larsloekke'))
#followers_acitivity('larsloekke',get_user_followers_info_dict('larsloekke'))
#mentions_sentimental_histogram('larsloekke')